﻿namespace SoftServeEvents.Contracts
{
    using System;
    using System.Linq;
    using SoftServeEvents.Domain.Genres;

    /// <summary>
    /// Interface for GenreService
    /// </summary>
    interface IGenre
    {
        /// <summary>
        /// Gets list of all Genres
        /// </summary>
        /// <returns>Return list of all Genres.</returns>
        IQueryable<Genre> GetAll();

        /// <summary>
        /// Delete specific Genre
        /// </summary>
        /// <param name="id">Genre id</param>
        void Delete(int id);

        /// <summary>
        /// Create new Genre.
        /// </summary>
        /// <param name="genre">New genre</param>
        void Create(Genre genre);

        /// <summary>
        /// Edit Genre
        /// </summary>
        /// <param name="genre">New Genre data</param>
        void Edit(Genre genre);

        /// <summary>
        /// Find a Genre by id
        /// </summary>
        /// <param name="id">id of Genre to find</param>
        /// <returns>Found Genre</returns>
        Genre FindById(int id);
    }
}
