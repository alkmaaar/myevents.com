﻿namespace SoftServeEvents.Contracts
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SoftServeEvents.Domain.Holidays;

    /// <summary>
    /// Interface for HolidayService
    /// </summary>
    interface IHoliday
    {
        /// <summary>
        /// Gets list of all holidays.
        /// </summary>
        /// <returns>Return list of all holidays.</returns>
        IQueryable<Holiday> GetAll();

        /// <summary>
        /// Delete specific holiday.
        /// </summary>
        /// <param name="id">holiday id</param>
        void Delete(int id);

        /// <summary>
        /// Create new holiday.
        /// </summary>
        /// <param name="place">New holiday</param>
        void Create(Holiday place);

        /// <summary>
        /// Edit holiday.
        /// </summary>
        /// <param name="place">New holiday data</param>
        void Edit(Holiday place);

        /// <summary>
        /// Find a holiday by id.
        /// </summary>
        /// <param name="id">id of holiday to find</param>
        /// <returns>Found holiday</returns>
        Holiday FindById(int id);
    }
}
