﻿namespace SoftServeEvents.Contracts
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Domain.Places;

    /// <summary>
    /// Interface for PlaceService
    /// </summary>
    interface IPlaceService
    {
        /// <summary>
        /// Gets list of all places.
        /// </summary>
        /// <returns>Return list of all places.</returns>
        IQueryable<Place> GetAll();

        /// <summary>
        /// Delete specific place.
        /// </summary>
        /// <param name="id">Tournament id</param>
        void Delete(int id);

        /// <summary>
        /// Create new place.
        /// </summary>
        /// <param name="place">New tournament</param>
        void Create(Place place);

        /// <summary>
        /// Edit place.
        /// </summary>
        /// <param name="place">New place data</param>
        void Edit(Place place);

        /// <summary>
        /// Find a place by id.
        /// </summary>
        /// <param name="id">id of place to find</param>
        /// <returns>Found place</returns>
        Place FindById(int id);
    }
}
