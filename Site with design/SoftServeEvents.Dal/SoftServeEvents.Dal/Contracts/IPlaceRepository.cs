﻿namespace SoftServeEvents.Dal.Contracts
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SoftServeEvents.Domain.Places;

    /// <summary>
    /// Defines specific contract for PlaceRepository
    /// </summary>
    interface IPlaceRepository
    {
        /// <summary>
        /// Gets all places.
        /// </summary>
        /// <returns>Collection of places from the repository.</returns>
        IQueryable<Place> FindAll();
    }
}
