﻿namespace SoftServeEvents.Dal.Contracts
{
    using SoftServeEvents.Domain.Holidays;
    using System.Linq;

    /// <summary>
    /// Defines specific contract for HolidayRepository
    /// </summary>
    interface IHolidayRepository
    {
        /// <summary>
        /// Gets all holidays.
        /// </summary>
        /// <returns>Collection of holidays from the repository.</returns>
        IQueryable<Holiday> FindAll();
    }
}
