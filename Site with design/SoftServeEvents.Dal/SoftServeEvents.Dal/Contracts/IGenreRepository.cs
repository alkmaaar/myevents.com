﻿namespace SoftServeEvents.Dal.Contracts
{
    using SoftServeEvents.Domain.Genres;
    using System.Linq;

    /// <summary>
    /// Defines specific contract for GenreRepository
    /// </summary>
    interface IGenreRepository
    {
        /// <summary>
        /// Gets all genres.
        /// </summary>
        /// <returns>Collection of genres from the repository.</returns>
        IQueryable<Genre> FindAll();
    }
}
