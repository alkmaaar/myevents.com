﻿namespace SoftServeEvents.Domain.Genres
{
    using System;
    using System.Collections.Generic;
    using SoftServeEvents.Domain.Holidays;

    /// <summary>
    /// Genre domain class.
    /// </summary>
    public class Genre
    {
        /// <summary>
        /// genre title
        /// </summary>
        private string _title;

        /// <summary>
        ///  Gets or sets a value indicating where Id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets a value indicating where Title.
        /// </summary>
        public string Title 
        {
            get
            {
                return this._title;
            }
            set
            {
                if (string.IsNullOrEmpty(value) || value.Length > Constants.Genre.MaxTitleLength)
                {
                    throw new ArgumentException("Invalid input of title field");
                }

                this._title = value;
            }
        }
    }
}
