﻿namespace SoftServeEvents.Domain.Places
{
    using System;
    using System.Collections.Generic;
    using SoftServeEvents.Domain.Holidays;

    /// <summary>
    /// Place domain class.
    /// </summary>
    public class Place
    {
        /// <summary>
        /// place title
        /// </summary>
        private string _title;

        /// <summary>
        /// place seats count
        /// </summary>
        private int _placeCount;

        /// <summary>
        /// place free seats count
        /// </summary>
        private int _freePlaceCount;

        /// <summary>
        ///  Gets or sets a value indicating where Id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets a value indicating where Title.
        /// </summary>
        public string Title {
            get
            {
                return this._title;
            }
            set 
            {
                if (string.IsNullOrEmpty(value) || value.Length > Constants.Place.MaxTitleLength)
                {
                    throw new ArgumentException("Invalid input of title field");
                }
            }
        }
        
        /// <summary>
        /// Gets or sets a value indicating where PlaceCount.
        /// </summary>
        public int PlaceCount 
        {
            get
            {
                return this._placeCount;

            }
            set
            {
                if (value < 0 || value > Constants.Place.MaxPlaceCount)
                {
                    throw new ArgumentException("Invalid input of placeCount field");
                }

                this._placeCount = value;
            }
        }
        
        /// <summary>
        /// Gets or sets a value indicating where FreePlaceCount.
        /// </summary>
        public int FreePlaceCount 
        {
            get
            {
                return this._freePlaceCount;
            }
            set
            { 
                if(value < 0 || value > Constants.Place.MaxFreePlaceCount)
                {
                    throw new ArgumentException("Invalid input of freePlaceCount field");
                }

                this._freePlaceCount = value;
            }
        }
    }
}
