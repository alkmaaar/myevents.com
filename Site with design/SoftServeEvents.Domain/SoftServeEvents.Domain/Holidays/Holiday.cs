﻿namespace SoftServeEvents.Domain.Holidays
{
    using System;
    using System.Collections.Generic;
    using SoftServeEvents.Domain.Genres;
    using SoftServeEvents.Domain.Places;

    /// <summary>
    /// Holiday domain class.
    /// </summary>
    public class Holiday
    {
        /// <summary>
        /// holiday title
        /// </summary>
        private string _title;

        /// <summary>
        ///  Gets or sets a value indicating where Id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets a value indicating where Title.
        /// </summary>
        public string Title 
        {
            get
            {
                return this._title;
            }
            set
            {
                if (string.IsNullOrEmpty(value) || value.Length > Constants.Holiday.MaxTitleLength)
                {
                    throw new ArgumentException("Invalid input of title field");
                }

                this._title = value;
            }
        }
 
    }
}
