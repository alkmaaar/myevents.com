﻿namespace SoftServeEvents.Domain
{
    using System;

    /// <summary>
    /// Container for glabals constants.
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// Container for holiday constants.
        /// </summary>
        public static class Holiday
        {
            /// <summary>
            /// constant defined the length of the title field
            /// </summary>
            public const int MaxTitleLength = 45;
        }

        /// <summary>
        /// Container for genre constants.
        /// </summary>
        public static class Genre
        {
            /// <summary>
            /// constant defined the length of the title field
            /// </summary>
            public const int MaxTitleLength = 45;
        }

        /// <summary>
        /// Contatiner for place constatnts.
        /// </summary>
        public static class Place
        {
            /// <summary>
            /// constant defined the length of the title field
            /// </summary>
            public const int MaxTitleLength = 45;

            /// <summary>
            /// constant defined the max value of the placeCount field
            /// </summary>
            public const int MaxPlaceCount = Int32.MaxValue;

            /// <summary>
            /// constant defined the max value of the freePlaceCount field
            /// </summary>
            public const int MaxFreePlaceCount = Int32.MaxValue;
        }
    }
}
