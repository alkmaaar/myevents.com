﻿namespace SoftServeEvents
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;

    /// <summary>
    /// Represents methods for registering of the routes 
    /// </summary>
    public class RouteConfig
    {
        /// <summary>
        /// Registers routes of the project 
        /// </summary>
        /// <param name="routes">Maps the specified URL route.</param>
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional });
        }
    }
}