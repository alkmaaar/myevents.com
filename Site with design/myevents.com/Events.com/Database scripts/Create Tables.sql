USE tempdb; 
IF OBJECT_ID('dbo.Genre', 'U') IS NULL 
	CREATE TABLE Genre(Id int IDENTITY(1,1) primary key NOT NULL, Title varchar(45) NOT NULL,)
IF OBJECT_ID('dbo.Client', 'U') IS NULL 
	CREATE TABLE Client(Id int IDENTITY(1,1) primary key NOT NULL, Name varchar(45) NOT NULL, PhoneNumber varchar(45) NOT NULL)
IF OBJECT_ID('dbo.Place', 'U') IS NULL 
	CREATE TABLE Place(Id int IDENTITY(1,1) primary key NOT NULL, Title varchar(45) NOT NULL, PlaceCount int NOT NULL, FreePlaceCount int NOT NULL)
IF OBJECT_ID('dbo.Event', 'U') IS NULL 
	CREATE TABLE Event(Id int IDENTITY(1,1) primary key NOT NULL, Title varchar(45) NOT NULL, PlaceId int NOT NULL, GenreId int NOT NULL,
	FOREIGN KEY (PlaceId) REFERENCES Place(Id) ON UPDATE CASCADE, FOREIGN KEY (GenreId) REFERENCES Genre(Id) ON UPDATE CASCADE)
IF OBJECT_ID('dbo.Orders', 'U') IS NULL 
	CREATE TABLE Orders(Id int IDENTITY(1,1) primary key NOT NULL, EventId int NOT NULL, ClientId int NOT NULL,
	FOREIGN KEY (EventId) REFERENCES Event(Id) ON UPDATE CASCADE, FOREIGN KEY (ClientId) REFERENCES Client(Id) ON UPDATE CASCADE)
