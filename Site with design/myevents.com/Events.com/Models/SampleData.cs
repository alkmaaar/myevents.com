﻿namespace SoftServeEvents.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Data.Entity;
    /*
    public class SampleData : DropCreateDatabaseIfModelChanges<EventsEntities>
    {
        protected override void Seed(EventsEntities context)
        {
            var genres = new List<Genre>
            {
                new Genre { Title = "Theatre" },
                new Genre { Title = "Cinema" },
                new Genre { Title = "Walks and tours" },
                new Genre { Title = "Poetry" },
                new Genre { Title = "Sports and wellbeing" },
                new Genre { Title = "Festivals" },
                new Genre { Title = "Concerts" },
                new Genre { Title = "Comedy" },
                new Genre { Title = "Conference" }
            };

            var clients = new List<Client>
            {
                new Client { Name = "ABBOTT, Mrs Rhoda Mary Rosa", PhoneNumber = "+38 066 854 47 45"},
                new Client { Name = "ABELSETH, Mr Olaus Jørgensen", PhoneNumber = "+38 066 854 47 43"},
                new Client { Name = "ALBIMONA, Mr Nassef Cassem", PhoneNumber = "+38 066 854 17 12"},
                new Client { Name = "ABELSETH, Miss Karen Marie", PhoneNumber = "+38 066 854 37 51"},
                new Client { Name = "ABRAHIM, Mrs Mary Sophie Halaut", PhoneNumber = "+38 066 854 27 61"},
                new Client { Name = "ANDERSSON, Miss Erna Alexandra", PhoneNumber = "+38 066 854 27 61"},
                new Client { Name = "ALLEN, Miss Elisabeth Walton", PhoneNumber = "+38 066 854 47 84"},
                new Client { Name = "ANDREWS, Miss Kornelia Theodosia", PhoneNumber = "+38 066 854 47 45"},
                new Client { Name = "ABELSON, Mrs Hannah", PhoneNumber = "+38 066 854 07 45"},
                new Client { Name = "ABRAHAMSSON, Mr Abraham August Johannes", PhoneNumber = "+38 066 854 97 45"},
                new Client { Name = "ANDERSON, Mr James", PhoneNumber = "+38 066 854 17 45"},
                new Client { Name = "ANDERSON, Mr Harry", PhoneNumber = "+38 066 854 27 43"},
                new Client { Name = "AKS, Master Frank Philip", PhoneNumber = "+38 066 854 47 44"},
                new Client { Name = "ALLEN, Mr Ernest Frederick", PhoneNumber = "+38 066 854 37 45"},
                new Client { Name = "AKS, Mrs Leah", PhoneNumber = "+38 066 854 47 46"},
                new Client { Name = "ANDERSEN-JENSEN, Miss Carla Christine Nielsine", PhoneNumber = "+38 066 854 57 47"}
            };

            var places = new List<Place>
            {
                new Place { Title = "Place1", PlaceCount = 3309, FreePlace = 25},
                new Place { Title = "Place2", PlaceCount = 4309, FreePlace = 25},
                new Place { Title = "Place3", PlaceCount = 5309, FreePlace = 25},
                new Place { Title = "Place4", PlaceCount = 2309, FreePlace = 25},
                new Place { Title = "Place5", PlaceCount = 1309, FreePlace = 25},
                new Place { Title = "Place6", PlaceCount = 2309, FreePlace = 25},
                new Place { Title = "Place7", PlaceCount = 3309, FreePlace = 25},
                new Place { Title = "Place8", PlaceCount = 4309, FreePlace = 25},
                new Place { Title = "Place9", PlaceCount = 1309, FreePlace = 25},
                new Place { Title = "Place10", PlaceCount = 2309, FreePlace = 25},
                new Place { Title = "Place11", PlaceCount = 6309, FreePlace = 25},
                new Place { Title = "Place12", PlaceCount = 2309, FreePlace = 25},
                new Place { Title = "Place13", PlaceCount = 1309, FreePlace = 25},
                new Place { Title = "Place14", PlaceCount = 2309, FreePlace = 25},
                new Place { Title = "Place15", PlaceCount = 2309, FreePlace = 25},
                new Place { Title = "Place16", PlaceCount = 3309, FreePlace = 25}
            };

            var events = new List<Event>
            {
                new Event { Title = "Event1", Path = "/Content/Images/Cinema.jpg", 
                    Genre = genres.Single(g => g.Title == "Theatre"), Place = places.Single(p => p.Title == "Place1") },  
                new Event { Title = "Event2", Path = "/Content/Images/Cinema.jpg", 
                    Genre = genres.Single(g => g.Title == "Theatre"), Place = places.Single(p => p.Title == "Place2") },
                new Event { Title = "Event3", Path = "/Content/Images/Cinema.jpg", 
                    Genre = genres.Single(g => g.Title == "Theatre"), Place = places.Single(p => p.Title == "Place1") },
                new Event { Title = "Event4", Path = "/Content/Images/Cinema.jpg", 
                    Genre = genres.Single(g => g.Title == "Theatre"), Place = places.Single(p => p.Title == "Place1") },
                new Event { Title = "Event5", Path = "/Content/Images/Cinema.jpg", 
                    Genre = genres.Single(g => g.Title == "Cinema"), Place = places.Single(p => p.Title == "Place2") },
                new Event { Title = "Event5", Path = "/Content/Images/Cinema.jpg", 
                    Genre = genres.Single(g => g.Title == "Cinema"), Place = places.Single(p => p.Title == "Place1") },
                new Event { Title = "Event6", Path = "/Content/Images/Cinema.jpg", 
                    Genre = genres.Single(g => g.Title == "Cinema"), Place = places.Single(p => p.Title == "Place1") },
                new Event { Title = "Event7", Path = "/Content/Images/Cinema.jpg", 
                    Genre = genres.Single(g => g.Title == "Cinema"), Place = places.Single(p => p.Title == "Place1") },
                new Event { Title = "Event8", Path = "/Content/Images/Cinema.jpg", 
                    Genre = genres.Single(g => g.Title == "Cinema"), Place = places.Single(p => p.Title == "Place1") },
                new Event { Title = "Event9", Path = "/Content/Images/Cinema.jpg", 
                    Genre = genres.Single(g => g.Title == "Cinema"), Place = places.Single(p => p.Title == "Place2") },
                new Event { Title = "Event10", Path = "/Content/Images/Cinema.jpg", 
                    Genre = genres.Single(g => g.Title == "Walks and tours"), Place = places.Single(p => p.Title == "Place2") },
                new Event { Title = "Event11", Path = "/Content/Images/Cinema.jpg", 
                    Genre = genres.Single(g => g.Title == "Walks and tours"), Place = places.Single(p => p.Title == "Place2") },
                new Event { Title = "Event12", Path = "/Content/Images/Cinema.jpg", 
                    Genre = genres.Single(g => g.Title == "Walks and tours"), Place = places.Single(p => p.Title == "Place2") },
                new Event { Title = "Event13", Path = "/Content/Images/Cinema.jpg", 
                    Genre = genres.Single(g => g.Title == "Poetry"), Place = places.Single(p => p.Title == "Place2") },
                new Event { Title = "Event14", Path = "/Content/Images/Cinema.jpg", 
                    Genre = genres.Single(g => g.Title == "Poetry"), Place = places.Single(p => p.Title == "Place2") },
                new Event { Title = "Event15", Path = "/Content/Images/Cinema.jpg", 
                    Genre = genres.Single(g => g.Title == "Poetry"), Place = places.Single(p => p.Title == "Place2") },
                new Event { Title = "Event16", Path = "/Content/Images/Cinema.jpg", 
                    Genre = genres.Single(g => g.Title == "Festivals"), Place = places.Single(p => p.Title == "Place3") },
                new Event { Title = "Event17", Path = "/Content/Images/Cinema.jpg", 
                    Genre = genres.Single(g => g.Title == "Festivals"), Place = places.Single(p => p.Title == "Place3") },
                new Event { Title = "Event19", Path = "/Content/Images/Cinema.jpg", 
                    Genre = genres.Single(g => g.Title == "Festivals"), Place = places.Single(p => p.Title == "Place3") },
                new Event { Title = "Event19", Path = "/Content/Images/Cinema.jpg", 
                    Genre = genres.Single(g => g.Title == "Festivals"), Place = places.Single(p => p.Title == "Place4") },
                new Event { Title = "Event20", Path = "/Content/Images/Cinema.jpg", 
                    Genre = genres.Single(g => g.Title == "Concerts"), Place = places.Single(p => p.Title == "Place4") },
                new Event { Title = "Event21", Path = "/Content/Images/Cinema.jpg", 
                    Genre = genres.Single(g => g.Title == "Concerts"), Place = places.Single(p => p.Title == "Place4") }
            };

            var orders = new List<Orders> 
            { 
                new Orders{Client = clients.Single(c => c.Name == "ABBOTT, Mrs Rhoda Mary Rosa"), Event = events.Single(e => e.Title == "Event11")},
                new Orders{Client = clients.Single(c => c.Name == "ANDERSEN-JENSEN, Miss Carla Christine Nielsine"), Event = events.Single(e => e.Title == "Event12")},
                new Orders{Client = clients.Single(c => c.Name == "ABBOTT, Mrs Rhoda Mary Rosa"), Event = events.Single(e => e.Title == "Event13")},
                new Orders{Client = clients.Single(c => c.Name == "ANDERSEN-JENSEN, Miss Carla Christine Nielsine"), Event = events.Single(e => e.Title == "Event4")},
                new Orders{Client = clients.Single(c => c.Name == "ALBIMONA, Mr Nassef Cassem"), Event = events.Single(e => e.Title == "Event5")},
                new Orders{Client = clients.Single(c => c.Name == "ALLEN, Mr Ernest Frederick"), Event = events.Single(e => e.Title == "Event6")},
                new Orders{Client = clients.Single(c => c.Name == "ALBIMONA, Mr Nassef Cassem"), Event = events.Single(e => e.Title == "Event7")},
                new Orders{Client = clients.Single(c => c.Name == "ABRAHAMSSON, Mr Abraham August Johannes"), Event = events.Single(e => e.Title == "Event8")},
                new Orders{Client = clients.Single(c => c.Name == "ABBOTT, Mrs Rhoda Mary Rosa"), Event = events.Single(e => e.Title == "Event1")},
                new Orders{Client = clients.Single(c => c.Name == "ALLEN, Mr Ernest Frederick"), Event = events.Single(e => e.Title == "Event11")},
                new Orders{Client = clients.Single(c => c.Name == "ALLEN, Mr Ernest Frederick"), Event = events.Single(e => e.Title == "Event1")},
                new Orders{Client = clients.Single(c => c.Name == "ALBIMONA, Mr Nassef Cassem"), Event = events.Single(e => e.Title == "Event2")},
                new Orders{Client = clients.Single(c => c.Name == "ABRAHIM, Mrs Mary Sophie Halaut"), Event = events.Single(e => e.Title == "Event11")},
                new Orders{Client = clients.Single(c => c.Name == "ABBOTT, Mrs Rhoda Mary Rosa"), Event = events.Single(e => e.Title == "Event2")},
                new Orders{Client = clients.Single(c => c.Name == "ABRAHIM, Mrs Mary Sophie Halaut"), Event = events.Single(e => e.Title == "Event8")},
                new Orders{Client = clients.Single(c => c.Name == "ABELSETH, Mr Olaus Jørgensen"), Event = events.Single(e => e.Title == "Event2")},
                new Orders{Client = clients.Single(c => c.Name == "ANDERSON, Mr James"), Event = events.Single(e => e.Title == "Event1")},
                new Orders{Client = clients.Single(c => c.Name == "ABELSETH, Mr Olaus Jørgensen"), Event = events.Single(e => e.Title == "Event3")},
                new Orders{Client = clients.Single(c => c.Name == "ANDERSON, Mr James"), Event = events.Single(e => e.Title == "Event15")},
                new Orders{Client = clients.Single(c => c.Name == "ABELSETH, Mr Olaus Jørgensen"), Event = events.Single(e => e.Title == "Event21")},
                new Orders{Client = clients.Single(c => c.Name == "ABRAHAMSSON, Mr Abraham August Johannes"), Event = events.Single(e => e.Title == "Event6")},
                new Orders{Client = clients.Single(c => c.Name == "ABRAHIM, Mrs Mary Sophie Halaut"), Event = events.Single(e => e.Title == "Event2")}
            };
        }
    }
    */
}