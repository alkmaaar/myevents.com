﻿namespace SoftServeEvents.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using SoftServeEvents.Models;

    /// <summary>
    /// Represents container of Genre and Event objects
    /// </summary>
    public class GenresAndEventsContainer
    {
        IQueryable<Genre> genres;
        IQueryable<Event> events;
        Genre genre;

        public GenresAndEventsContainer(IQueryable<Genre> genres, IQueryable<Event> events, Genre genre = null)
        {
            this.genres = genres;
            this.events = events;
            this.genre = genre;
        }

        public IQueryable<Genre> Genres
        {
            get
            {
                return this.genres;
            }
        }

        public IQueryable<Event> Events
        {
            get
            {
                return this.events;
            }
        }

        public Genre Genre
        {
            get
            {
                return genre;
            }
        }
    }
}