﻿namespace SoftServeEvents.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Entity;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using SoftServeEvents.Models;

    /// <summary>
    /// Represents methods for Event Page
    /// </summary>
    public class AllEventController : Controller
    {
        /// <summary>
        /// Database context for interaction with database
        /// </summary>
        private EventsDbEntities db = new EventsDbEntities();

        //
        // GET: /AllEvent/
        /// <summary>
        /// Constructs the logic of the Index page
        /// </summary>
        /// <returns>Returns a control that acts as a container</returns>
        public ActionResult Index()
        {
            var events = this.db.Events.Include(e => e.Genre).Include(e => e.Place);
            return View(events.ToList());
        }

        //
        // GET: /AllEvent/Details/5
        /// <summary>
        /// Constructs the logic of the Details page
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns a control that acts as a container</returns>
        public ActionResult Details(int id = 0)
        {
            Event myEvent = this.db.Events.Find(id);
            if (myEvent == null)
            {
                return this.HttpNotFound();
            }
            return View(myEvent);
        }

        //
        // GET: /AllEvent/Create
        /// <summary>
        /// Constructs the logic of the Create page
        /// </summary>
        /// <returns>Returns a control that acts as a container</returns>
        public ActionResult Create()
        {
            ViewBag.GenreId = new SelectList(this.db.Genres, "Id", "Title");
            ViewBag.PlaceId = new SelectList(this.db.Places, "Id", "Title");
            return View();
        }

        //
        // POST: /AllEvent/Create
        /// <summary>
        /// Constructs the logic of the Create page
        /// </summary>
        /// <param name="myEvent">Event to create</param>
        /// <returns>Returns a control that acts as a container</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Event myEvent)
        {
            if (ModelState.IsValid)
            {
                List<int> ids = new List<int>();
                foreach (Event e in this.db.Events)
                {
                    ids.Add(e.Id);
                }

                while (ids.Contains(myEvent.Id))
                {
                    myEvent.Id++;
                }

                this.db.Events.Add(myEvent);
                this.db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.GenreId = new SelectList(this.db.Genres, "Id", "Title", myEvent.GenreId);
            ViewBag.PlaceId = new SelectList(this.db.Places, "Id", "Title", myEvent.PlaceId);
            return View(myEvent);
        }

        //
        // GET: /AllEvent/Edit/5
        /// <summary>
        /// Constructs the logic of the Edit page
        /// </summary>
        /// <param name="id">Identifier</param>
        /// <returns>Returns a control that acts as a container</returns>
        public ActionResult Edit(int id = 0)
        {
            Event myEvent = this.db.Events.Find(id);
            if (myEvent == null)
            {
                return HttpNotFound();
            }
            ViewBag.GenreId = new SelectList(this.db.Genres, "Id", "Title", myEvent.GenreId);
            ViewBag.PlaceId = new SelectList(this.db.Places, "Id", "Title", myEvent.PlaceId);
            return View(myEvent);
        }

        //
        // POST: /AllEvent/Edit/5
        /// <summary>
        /// Constructs the logic of the Edit page
        /// </summary>
        /// <param name="myEvent">Event to edit</param>
        /// <returns>Returns a control that acts as a container</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Event myEvent)
        {
            if (ModelState.IsValid)
            {
                this.db.Entry(myEvent).State = EntityState.Modified;
                this.db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.GenreId = new SelectList(this.db.Genres, "Id", "Title", myEvent.GenreId);
            ViewBag.PlaceId = new SelectList(this.db.Places, "Id", "Title", myEvent.PlaceId);
            return View(myEvent);
        }

        //
        // GET: /AllEvent/Delete/5
        /// <summary>
        /// Constructs the logic of the Delete page
        /// </summary>
        /// <param name="id">Identifier</param>
        /// <returns>Returns a control that acts as a container</returns>
        public ActionResult Delete(int id = 0)
        {
            Event myEvent = this.db.Events.Find(id);
            if (myEvent == null)
            {
                return HttpNotFound();
            }
            return View(myEvent);
        }

        //
        // POST: /AllEvent/Delete/5
        /// <summary>
        /// Constructs the logic of the Delete confirm page
        /// </summary>
        /// <param name="id">Identifier</param>
        /// <returns>Returns a control that acts as a container</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Event myEvent = this.db.Events.Find(id);
            this.db.Events.Remove(myEvent);
            this.db.SaveChanges();
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Release the resorces 
        /// </summary>
        /// <param name="disposing">Disposing flag</param>
        protected override void Dispose(bool disposing)
        {
            this.db.Dispose();
            base.Dispose(disposing);
        }
    }
}