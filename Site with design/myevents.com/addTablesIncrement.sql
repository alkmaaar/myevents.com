﻿IF OBJECT_ID('dbo.Genre', 'U') IS NULL 
	CREATE TABLE Genre(Id int IDENTITY(1,1) primary key NOT NULL, Title varchar(45) NOT NULL,)
IF OBJECT_ID('dbo.Client', 'U') IS NULL 
	CREATE TABLE Client(Id int IDENTITY(1,1) primary key NOT NULL, Name varchar(45) NOT NULL, PhoneNumber varchar(45) NOT NULL)
IF OBJECT_ID('dbo.Place', 'U') IS NULL 
	CREATE TABLE Place(Id int IDENTITY(1,1) primary key NOT NULL, Title varchar(45) NOT NULL, PlaceCount int NOT NULL, FreePlaceCount int NOT NULL)
IF OBJECT_ID('dbo.Event', 'U') IS NULL 
	CREATE TABLE Event(Id int IDENTITY(1,1) primary key NOT NULL, Title varchar(45) NOT NULL, PlaceId int NOT NULL, GenreId int NOT NULL,
	FOREIGN KEY (PlaceId) REFERENCES Place(Id) ON UPDATE CASCADE, FOREIGN KEY (GenreId) REFERENCES Genre(Id) ON UPDATE CASCADE)
IF OBJECT_ID('dbo.Orders', 'U') IS NULL 
	CREATE TABLE Orders(Id int IDENTITY(1,1) primary key NOT NULL, EventId int NOT NULL, ClientId int NOT NULL,
	FOREIGN KEY (EventId) REFERENCES Event(Id) ON UPDATE CASCADE, FOREIGN KEY (ClientId) REFERENCES Client(Id) ON UPDATE CASCADE)
INSERT INTO dbo.Genre(Title) VALUES ('Theatre')
INSERT INTO dbo.Genre(Title) VALUES ('Cinema')
INSERT INTO dbo.Genre(Title) VALUES ('Walks and tours')
INSERT INTO dbo.Genre(Title) VALUES ('Poetry')
INSERT INTO dbo.Genre(Title) VALUES ('Sports and wellbeing')
INSERT INTO dbo.Client(Name, PhoneNumber) VALUES ('Алиса Черникова', '8-913-903-59-88')
INSERT INTO dbo.Client(Name, PhoneNumber) VALUES ('Олег Туманов', '8-913-903-59-88')
INSERT INTO dbo.Client(Name, PhoneNumber) VALUES ('Андрей Богдашев', '8-913-903-59-88')
INSERT INTO dbo.Client(Name, PhoneNumber) VALUES ('Петр Сидоров', '8-913-903-59-88')
INSERT INTO dbo.Client(Name, PhoneNumber) VALUES ('Влад Туманов', '8-913-903-59-88')
INSERT INTO dbo.Client(Name, PhoneNumber) VALUES ('Антон Снытко', '8-913-903-59-88')
INSERT INTO dbo.Client(Name, PhoneNumber) VALUES ('Аня Суворина', '8-913-903-59-88')
INSERT INTO dbo.Client(Name, PhoneNumber) VALUES ('Антон Байкалов', '8-913-903-59-88')
INSERT INTO dbo.Client(Name, PhoneNumber) VALUES ('Алёна Шорина', '8-961-220-52-63')
INSERT INTO dbo.Client(Name, PhoneNumber) VALUES ('Андрей Давыдов', '8-961-220-52-63')
INSERT INTO dbo.Client(Name, PhoneNumber) VALUES ('Виталя Ладунов', '8-961-220-52-63')
INSERT INTO dbo.Place(Title, PlaceCount, FreePlaceCount) VALUES ('Place1', 2500, 20)
INSERT INTO dbo.Place(Title, PlaceCount, FreePlaceCount) VALUES ('Place2', 3500, 10)
INSERT INTO dbo.Place(Title, PlaceCount, FreePlaceCount) VALUES ('Place3', 4500, 30)
INSERT INTO dbo.Place(Title, PlaceCount, FreePlaceCount) VALUES ('Place4', 1500, 120)
INSERT INTO dbo.Place(Title, PlaceCount, FreePlaceCount) VALUES ('Place5', 1500, 220)
INSERT INTO dbo.Place(Title, PlaceCount, FreePlaceCount) VALUES ('Place6', 7500, 250)
INSERT INTO dbo.Place(Title, PlaceCount, FreePlaceCount) VALUES ('Place7', 2400, 210)
INSERT INTO dbo.Place(Title, PlaceCount, FreePlaceCount) VALUES ('Place8', 2300, 220)
INSERT INTO dbo.Place(Title, PlaceCount, FreePlaceCount) VALUES ('Place9', 200, 120)
INSERT INTO dbo.Place(Title, PlaceCount, FreePlaceCount) VALUES ('Place10', 1100, 620)
INSERT INTO dbo.Place(Title, PlaceCount, FreePlaceCount) VALUES ('Place11', 500, 220)
INSERT INTO dbo.Place(Title, PlaceCount, FreePlaceCount) VALUES ('Place12', 300, 220)
INSERT INTO dbo.Event(Title, PlaceId, GenreId) VALUES ('Event1', 1, 1)
INSERT INTO dbo.Event(Title, PlaceId, GenreId) VALUES ('Event2', 2, 2)
INSERT INTO dbo.Event(Title, PlaceId, GenreId) VALUES ('Event3', 3, 2)
INSERT INTO dbo.Event(Title, PlaceId, GenreId) VALUES ('Event4', 4, 3)
INSERT INTO dbo.Event(Title, PlaceId, GenreId) VALUES ('Event5', 5, 4)
INSERT INTO dbo.Event(Title, PlaceId, GenreId) VALUES ('Event6', 6, 5)
INSERT INTO dbo.Event(Title, PlaceId, GenreId) VALUES ('Event8', 2, 2)
INSERT INTO dbo.Event(Title, PlaceId, GenreId) VALUES ('Event9', 1, 1)
INSERT INTO dbo.Event(Title, PlaceId, GenreId) VALUES ('Event10', 2, 2)
INSERT INTO dbo.Event(Title, PlaceId, GenreId) VALUES ('Event11', 1, 1)
INSERT INTO dbo.Event(Title, PlaceId, GenreId) VALUES ('Event12', 2, 2)
INSERT INTO dbo.Event(Title, PlaceId, GenreId) VALUES ('Event13', 1, 3)
INSERT INTO dbo.Orders(EventId, ClientId) VALUES (1, 1)
INSERT INTO dbo.Orders(EventId, ClientId) VALUES (2, 2)
INSERT INTO dbo.Orders(EventId, ClientId) VALUES (3, 3)
INSERT INTO dbo.Orders(EventId, ClientId) VALUES (4, 4)
INSERT INTO dbo.Orders(EventId, ClientId) VALUES (5, 1)
INSERT INTO dbo.Orders(EventId, ClientId) VALUES (6, 6)
INSERT INTO dbo.Orders(EventId, ClientId) VALUES (2, 8)
INSERT INTO dbo.Orders(EventId, ClientId) VALUES (3, 9)
INSERT INTO dbo.Orders(EventId, ClientId) VALUES (4, 1)
INSERT INTO dbo.Orders(EventId, ClientId) VALUES (1, 1)