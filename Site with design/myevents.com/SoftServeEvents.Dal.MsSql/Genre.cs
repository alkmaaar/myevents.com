﻿namespace SoftServeEvents.Dal.MsSql
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// DAL genre model
    /// </summary>
    public partial class Genre
    {
        /// <summary>
        /// Gets or sets id of genre.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets title of genre.
        /// </summary>
        [Required]
        [StringLength(45)]
        public string Title { get; set; }
    }
}
