﻿namespace SoftServeEvents.Dal.MsSql
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// DAL holiday model
    /// </summary>
    class Holiday
    {
        /// <summary>
        /// Gets or sets a value indicating where Id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets a value indicating where Title.
        /// </summary>
        public string Title { get; set; }
    }
}
