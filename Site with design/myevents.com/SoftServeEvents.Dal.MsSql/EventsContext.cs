﻿namespace SoftServeEvents.Dal.MsSql
{
    using System.Data.Entity;

    /// <summary>
    /// events database context
    /// </summary>
    internal partial class EventsContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EventsContext" /> class.
        /// </summary>
        public EventsContext()
            : base("EventsContext")
        {
            var instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }

        /// <summary>
        /// Gets or sets the genre table.
        /// </summary>
        public virtual DbSet<Genre> Genres { get; set; }

        /// <summary>
        /// Gets or sets the place table.
        /// </summary>
        public virtual DbSet<Place> Places { get; set; }

        /// <summary>
        /// Gets or sets the holiday table.
        /// </summary>
        public virtual DbSet<Holiday> Holidays { get; set; }

        /// <summary>
        /// configure models if needed
        /// </summary>
        /// <param name="modelBuilder">model builder</param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
