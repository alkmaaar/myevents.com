﻿namespace SoftServeEvents.Dal.MsSql
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    /// <summary>
    /// DAL place model
    /// </summary>
    public partial class Place 
    {
        /// <summary>
        /// Gets or sets id of place.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets title of place.
        /// </summary>
        [Required]
        [StringLength(45)]
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets number of seats.
        /// </summary>
        [Required]
        public int PlaceCount { get; set; }

        /// <summary>
        /// Gets or sets number of free seats. 
        /// </summary>
        [Required]
        public int FreePlaceCount { get; set; }
    }
}
