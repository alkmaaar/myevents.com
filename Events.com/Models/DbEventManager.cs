﻿namespace SoftServeEvents.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using SoftServeEvents.Models;

    public class DbEventManager
    {
        private EventsDbEntities db = new EventsDbEntities();


        public GenresAndEventsContainer GetAllPlacesoOfEvent()
        {
            IQueryable<Event> events = this.GetEventsFromDb();
            IQueryable<Genre> genres = this.GetGenresFromDb();
            return new GenresAndEventsContainer(genres, events);
        }

        private IQueryable<Genre> GetGenresFromDb()
        {
            return db.Genres;
        }

        public GenresAndEventsContainer GetAllEventsOfCurrentGenre(string genre)
        {
            Genre genreModel = db.Genres.Include("Events").Single(g => g.Title == genre);
            IQueryable<Event> events = this.GetEventsFromDb();
            IQueryable<Genre> genres = this.GetGenresFromDb();
            return new GenresAndEventsContainer(genres, events, genreModel);
        }

        private IQueryable<Event> GetEventsFromDb()
        {
            return db.Events;
        }

    }
}