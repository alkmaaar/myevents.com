﻿namespace SoftServeEvents.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Entity;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using SoftServeEvents.Models;

    public class OrderController : Controller
    {
        /// <summary>
        /// Database context for interaction with database
        /// </summary>
        private EventsDbEntities db = new EventsDbEntities();

        //
        // GET: /Order/
        /// <summary>
        /// Constructs the logic of the Index page
        /// </summary>
        /// <returns>Returns a control that acts as a container</returns>
        public ActionResult Index()
        {
            var orders = this.db.Orders.Include(o => o.Client).Include(o => o.Event);
            return View(orders.ToList());
        }

        //
        // GET: /Order/Details/5
        /// <summary>
        /// Constructs the logic of the Details page
        /// </summary>
        /// <param name="id">Identifier</param>
        /// <returns>Returns a control that acts as a container</returns>
        public ActionResult Details(int id = 0)
        {
            Order order = this.db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }

            return View(order);
        }

        //
        // GET: /Order/Create
        /// <summary>
        /// Constructs the logic of the Create page
        /// </summary>
        /// <returns>Returns a control that acts as a container</returns>
        public ActionResult Create()
        {
            ViewBag.ClientId = new SelectList(this.db.Clients, "Id", "Name");
            ViewBag.EventId = new SelectList(this.db.Events, "Id", "Title");
            return View();
        }

        //
        // POST: /Order/Create
        /// <summary>
        /// Constructs the logic of the Create page
        /// </summary>
        /// <param name="order">Order to add</param>
        /// <returns>Returns a control that acts as a container</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Order order)
        {
            if (ModelState.IsValid)
            {
                List<int> ids = new List<int>();
                foreach (Order o in this.db.Orders)
                {
                    ids.Add(o.Id);
                }

                while (ids.Contains(order.Id))
                {
                    order.Id++;
                }

                Event ev = this.db.Events.Find(order.EventId);
                ev.Place.FreePlaceCount--;

                this.db.Orders.Add(order);
                this.db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ClientId = new SelectList(this.db.Clients, "Id", "Name", order.ClientId);
            ViewBag.EventId = new SelectList(this.db.Events, "Id", "Title", order.EventId);
            return View(order);
        }

        //
        // GET: /Order/Edit/5
        /// <summary>
        /// Constructs the logic of the Edit page
        /// </summary>
        /// <param name="id">Identifier</param>
        /// <returns>Returns a control that acts as a container</returns>
        public ActionResult Edit(int id = 0)
        {
            Order order = this.db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }

            ViewBag.ClientId = new SelectList(this.db.Clients, "Id", "Name", order.ClientId);
            ViewBag.EventId = new SelectList(this.db.Events, "Id", "Title", order.EventId);
            return View(order);
        }

        //
        // POST: /Order/Edit/5
        /// <summary>
        /// Constructs the logic of the Edit page
        /// </summary>
        /// <param name="order">Order to edit</param>
        /// <returns>Returns a control that acts as a container</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Order order)
        {
            if (ModelState.IsValid)
            {
                this.db.Entry(order).State = EntityState.Modified;
                this.db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ClientId = new SelectList(this.db.Clients, "Id", "Name", order.ClientId);
            ViewBag.EventId = new SelectList(this.db.Events, "Id", "Title", order.EventId);
            return View(order);
        }

        //
        // GET: /Order/Delete/5
        /// <summary>
        /// Constructs the logic of the Delete page
        /// </summary>
        /// <param name="id">Identifier</param>
        /// <returns>Returns a control that acts as a container</returns>
        public ActionResult Delete(int id = 0)
        {
            Order order = this.db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }

            return View(order);
        }

        //
        // POST: /Order/Delete/5
        /// <summary>
        /// Constructs the logic of the Delete confirm page
        /// </summary>
        /// <param name="id">Identifier</param>
        /// <returns>Returns a control that acts as a container</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Order order = this.db.Orders.Find(id);
            this.db.Orders.Remove(order);
            this.db.SaveChanges();
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Release the recources 
        /// </summary>
        /// <param name="disposing">Disposing flag</param>
        protected override void Dispose(bool disposing)
        {
            this.db.Dispose();
            base.Dispose(disposing);
        }
    }
}