﻿namespace SoftServeEvents.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using SoftServeEvents.Models;
    using System.Collections;

    /// <summary>
    /// Represents methods for processing homepage
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// Using for interaction with database 
        /// </summary>
        DbEventManager dbManager = new DbEventManager();

        /// <summary>
        /// Constructs the Index page 
        /// </summary>
        /// <returns>Returns a control that acts as a container</returns>
        public ActionResult Index()
        {
            GenresAndEventsContainer container = this.dbManager.GetAllPlacesoOfEvent();
            return View(container);
        }

        /// <summary>
        /// Constructs the Browse page 
        /// </summary>
        /// <param name="genre">New genre to render</param>
        /// <returns>Returns a control that acts as a container</returns>
        public ActionResult Browse(string genre)
        {
            GenresAndEventsContainer container = this.dbManager.GetAllEventsOfCurrentGenre(genre);
            return View(container);
        }
    }
}
