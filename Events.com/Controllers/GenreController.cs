﻿namespace SoftServeEvents.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Entity;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using SoftServeEvents.Models;

    public class GenreController : Controller
    {
        /// <summary>
        /// Database context for interaction with database
        /// </summary>
        private EventsDbEntities db = new EventsDbEntities();

        //
        // GET: /Genre/
        /// <summary>
        /// Constructs the logic of the Index page
        /// </summary>
        /// <returns>Returns a control that acts as a container</returns>
        public ActionResult Index()
        {
            return View(this.db.Genres.ToList());
        }

        //
        // GET: /Genre/Details/5
        /// <summary>
        /// Constructs the logic of the Details page
        /// </summary>
        /// <param name="id">Identifier</param>
        /// <returns>Returns a control that acts as a container</returns>
        public ActionResult Details(int id = 0)
        {
            Genre genre = this.db.Genres.Find(id);
            if (genre == null)
            {
                return HttpNotFound();
            }
            return View(genre);
        }

        //
        // GET: /Genre/Create
        /// <summary>
        /// Constructs the logic of the Create page
        /// </summary>
        /// <returns>Returns a control that acts as a container</returns>
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Genre/Create
        /// <summary>
        /// Constructs the logic of the Create page
        /// </summary>
        /// <param name="genre">Genre to add</param>
        /// <returns>Returns a control that acts as a container</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Genre genre)
        {
            if (ModelState.IsValid)
            {
                List<int> ids = new List<int>();
                foreach (Genre g in this.db.Genres)
                {
                    ids.Add(g.Id);
                }

                while (ids.Contains(genre.Id))
                {
                    genre.Id++;
                }
                
                this.db.Genres.Add(genre);
                this.db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(genre);
        }

        //
        // GET: /Genre/Edit/5
        /// <summary>
        /// Constructs the logic of the Edit page
        /// </summary>
        /// <param name="id">Identifier</param>
        /// <returns>Returns a control that acts as a container</returns>
        public ActionResult Edit(int id = 0)
        {
            Genre genre = this.db.Genres.Find(id);
            if (genre == null)
            {
                return HttpNotFound();
            }
            return View(genre);
        }

        //
        // POST: /Genre/Edit/5
        /// <summary>
        /// Constructs the logic of the Edit page
        /// </summary>
        /// <param name="genre">Genre to edit</param>
        /// <returns>Returns a control that acts as a container</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Genre genre)
        {
            if (ModelState.IsValid)
            {
                this.db.Entry(genre).State = EntityState.Modified;
                this.db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(genre);
        }

        //
        // GET: /Genre/Delete/5
        /// <summary>
        /// Constructs the logic of the Delete page
        /// </summary>
        /// <param name="id">Identifier</param>
        /// <returns>Returns a control that acts as a container</returns>
        public ActionResult Delete(int id = 0)
        {
            Genre genre = this.db.Genres.Find(id);
            if (genre == null)
            {
                return HttpNotFound();
            }
            return View(genre);
        }

        //
        // POST: /Genre/Delete/5
        /// <summary>
        /// Constructs the logic of the Delete confirm page
        /// </summary>
        /// <param name="id">Identifier</param>
        /// <returns>Returns a control that acts as a container</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Genre genre = this.db.Genres.Find(id);
            this.db.Genres.Remove(genre);
            this.db.SaveChanges();
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Release the recources
        /// </summary>
        /// <param name="disposing">Disposing file</param>
        protected override void Dispose(bool disposing)
        {
            this.db.Dispose();
            base.Dispose(disposing);
        }
    }
}