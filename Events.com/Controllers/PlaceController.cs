﻿namespace SoftServeEvents.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Entity;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using SoftServeEvents.Models;

    /// <summary>
    /// Represents methods for work with a Place table 
    /// </summary>
    public class PlaceController : Controller
    {
        /// <summary>
        /// Database context for interaction with database
        /// </summary>
        private EventsDbEntities db = new EventsDbEntities();

        //
        // GET: /Place/
        /// <summary>
        /// Construct logic of the Index page 
        /// </summary>
        /// <returns>Returns a control that acts as a container</returns>
        public ActionResult Index()
        {
            return View(this.db.Places.ToList());
        }

        //
        // GET: /Place/Details/5
        /// <summary>
        /// Constructs the logic of the Place page
        /// </summary>
        /// <param name="id">Identifier</param>
        /// <returns>Returns a control that acts as a container</returns>
        public ActionResult Details(int id = 0)
        {
            Place place = this.db.Places.Find(id);
            if (place == null)
            {
                return HttpNotFound();
            }

            return View(place);
        }

        //
        // GET: /Place/Create
        /// <summary>
        /// Constructs the logic of the Create page
        /// </summary>
        /// <returns>Returns a control that acts as a container</returns>
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Place/Create
        /// <summary>
        /// Constructs the logic of the Create page
        /// </summary>
        /// <param name="place">New place to add</param>
        /// <returns>Returns a control that acts as a container</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Place place)
        {
            if (ModelState.IsValid)
            {
                List<int> ids = new List<int>();
                foreach (Place p in this.db.Places)
                {
                    ids.Add(p.Id);
                }

                while (ids.Contains(place.Id))
                {
                    place.Id++;
                }

                this.db.Places.Add(place);
                this.db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(place);
        }

        //
        // GET: /Place/Edit/5
        /// <summary>
        /// Constructs the logic of the Edit page
        /// </summary>
        /// <param name="id">Identifier</param>
        /// <returns>Returns a control that acts as a container</returns>
        public ActionResult Edit(int id = 0)
        {
            Place place = this.db.Places.Find(id);
            if (place == null)
            {
                return HttpNotFound();
            }

            return View(place);
        }

        //
        // POST: /Place/Edit/5
        /// <summary>
        /// Constructs the logic of the Edit page
        /// </summary>
        /// <param name="place">New place to add</param>
        /// <returns>Returns a control that acts as a container</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Place place)
        {
            if (ModelState.IsValid)
            {
                this.db.Entry(place).State = EntityState.Modified;
                this.db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(place);
        }

        //
        // GET: /Place/Delete/5
        /// <summary>
        /// Constructs the logic of the Delete page
        /// </summary>
        /// <param name="id">Identifier</param>
        /// <returns>Returns a control that acts as a container</returns>
        public ActionResult Delete(int id = 0)
        {
            Place place = db.Places.Find(id);
            if (place == null)
            {
                return HttpNotFound();
            }

            return View(place);
        }

        //
        // POST: /Place/Delete/5
        /// <summary>
        /// Constructs the logic of the Delete confirm page
        /// </summary>
        /// <param name="id">Identifier</param>
        /// <returns>Returns a control that acts as a container</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Place place = this.db.Places.Find(id);
            this.db.Places.Remove(place);
            this.db.SaveChanges();
            return RedirectToAction("Index");
        }
        /// <summary>
        /// Release the resources 
        /// </summary>
        /// <param name="disposing"><disposing flag/param>
        protected override void Dispose(bool disposing)
        {
            this.db.Dispose();
            base.Dispose(disposing);
        }
    }
}