﻿namespace SoftServeEvents.Controllers
{
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Entity;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using SoftServeEvents.Models;

    /// <summary>
    /// Represents the methods for Client page
    /// </summary>
    public class ClientController : Controller
    {
        /// <summary>
        /// Database context for interaction with database
        /// </summary>
        private EventsDbEntities db = new EventsDbEntities();

        //
        // GET: /Client/
        /// <summary>
        /// Constructs the logic of the Index page
        /// </summary>
        /// <returns>Returns a control that acts as a container</returns>
        public ActionResult Index()
        {
            return View(this.db.Clients.ToList());
        }

        //
        // GET: /Client/Details/5
        /// <summary>
        /// Constructs the logic of the Details page
        /// </summary>
        /// <param name="id">Identifier</param>
        /// <returns>Returns a control that acts as a container</returns>
        public ActionResult Details(int id = 0)
        {
            Client client = this.db.Clients.Find(id);
            if (client == null)
            {
                return HttpNotFound();
            }
            return View(client);
        }

        //
        // GET: /Client/Create
        /// <summary>
        /// Constructs the logic of the Create page
        /// </summary>
        /// <returns>Returns a control that acts as a container</returns>
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Client/Create
        /// <summary>
        /// Constructs the logic of the Create page
        /// </summary>
        /// <param name="client">Client to add</param>
        /// <returns>Returns a control that acts as a container</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Client client)
        {
            if (ModelState.IsValid)
            {
                List<int> ids = new List<int>();
                foreach (Client c in this.db.Clients)
                {
                    ids.Add(c.Id);
                }

                while (ids.Contains(client.Id))
                {
                    client.Id++;
                }

                this.db.Clients.Add(client);
                this.db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(client);
        }

        //
        // GET: /Client/Edit/5
        /// <summary>
        /// Constructs the logic of the Edit page
        /// </summary>
        /// <param name="id">Identifier</param>
        /// <returns>Returns a control that acts as a container</returns>
        public ActionResult Edit(int id = 0)
        {
            Client client = this.db.Clients.Find(id);
            if (client == null)
            {
                return HttpNotFound();
            }

            return View(client);
        }

        //
        // POST: /Client/Edit/5
        /// <summary>
        /// Constructs the logic of the Edit page
        /// </summary>
        /// <param name="client">Client to edit</param>
        /// <returns>Returns a control that acts as a container</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Client client)
        {
            if (ModelState.IsValid)
            {
                this.db.Entry(client).State = EntityState.Modified;
                this.db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(client);
        }

        //
        // GET: /Client/Delete/5
        /// <summary>
        /// Constructs the logic of the Delete page
        /// </summary>
        /// <param name="id">Identifier</param>
        /// <returns>Returns a control that acts as a container</returns>
        public ActionResult Delete(int id = 0)
        {
            Client client = this.db.Clients.Find(id);
            if (client == null)
            {
                return HttpNotFound();
            }

            return View(client);
        }

        //
        // POST: /Client/Delete/5
        /// <summary>
        /// Constructs the logic of the Delete confirm page
        /// </summary>
        /// <param name="id">Identifier</param>
        /// <returns>Returns a control that acts as a container</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Client client = this.db.Clients.Find(id);
            this.db.Clients.Remove(client);
            this.db.SaveChanges();
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Release the recources 
        /// </summary>
        /// <param name="disposing">Disposing flag</param>
        protected override void Dispose(bool disposing)
        {
            this.db.Dispose();
            base.Dispose(disposing);
        }
    }
}