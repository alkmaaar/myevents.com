﻿namespace SoftServeEvents.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using SoftServeEvents.Models;

    /// <summary>
    /// Represents the Event page methods
    /// </summary>
    public class EventDetailsController : Controller
    {
        /// <summary>
        /// Database context for interaction with database
        /// </summary>
        EventsDbEntities db = new EventsDbEntities();

        /// <summary>
        /// Construct logic of the Show Event page 
        /// </summary>
        /// <param name="placeId">Identifier of the Place row</param>
        /// <param name="currentEvent">Event which is presented</param>
        /// <returns>Returns a control that acts as a container</returns>
        public ActionResult ShowEvent(int placeId, string currentEvent)
        {
            var place = this.db.Places.Include("Events").Single(e => e.Id == placeId);
            ViewBag.CurrentEvent = currentEvent;
            return View(place);
        }
    }
}
